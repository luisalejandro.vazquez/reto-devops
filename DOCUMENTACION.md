# DOCUMENTACION LV

Documento que posee breve descripcion de los pasos ejecutados en el reto

## Paso 1: Dockerize la aplicación.

Se inicia la construccion de la imagen docker, archivo "Dockerfile" del repositorio.

## Paso 2: Docker-Compose

Se construye el archivo "docker-compose" que se encuentra en este repositorio.
En él, se muestra la ejecucion de dos docker, el primero denominamo "nodejs" siendo la app, y el segundo el nginx.

### Paso 3: CICD

Se contruye el test de la app .gitlab-ci

### Paso 4: Deploy en Kubernetes

Este proceso lo he llevado a cabo en mi cuenta de Google Cloud:

1) Configuracion de Google SDK Cloud para la terminal.
2) Instalo los componentes kubectl para gcloud.
3) Tomando el Dockerfile ya realizado en el paso uno registro creo y registro la imagen en gcloud.

Usando el comando docker build -t gcr.io/crafty-eye-276000/nodejs ->gcr.io se refiere al registro de gcloud

4) Subo la imagen del contenedor.

	Se autentica gcloud con el registro de imagenes de google.
	gcloud auth configure-docker

	Luego, docker push gcr.io/crafty-eye-276000/nodejs

5) Procedo a crear el cluster de contenedores:

	gcloud set project
	gcloud set compute/zone

	Para continuar, creo el cluster de kubernetes:
	gcloud container clusters create retodevops --num-nodes=2 (2 nodos)
	
	Implemento aplicacion en pod de kubernetes:
	kubectl create deplyment reto-devops-pod --image guardada en el regitry cloud de google.

	Para finalizar configuro el autoscale:
	kubectl autoscale deployment reto-devops-pod --cpu-percent=50 --min=1 --max=10

	Por ultimo extraigo el archivo yaml de gcloud "implementacion.yaml"

### Paso Makefile:

	make build
	construira la imagen y desplegará docker compose para nginx y nodejs
