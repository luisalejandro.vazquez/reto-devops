FROM node:10-alpine

RUN apk update
COPY . .
RUN npm install
EXPOSE 3000
ENV USER luis
RUN adduser -D $USER
USER $USER
CMD ["node","index.js"]

